**1-(terminal)**

    
`npm install --save redux`

**2-(index.js)**

	import {createStore} from "redux";

**3-(src)**

	create [store] folder

**4-(store)**

 	   create reducer.js

**5-(reducer.js)**                    
> initial state is default state for store | first run   reducer will accept [state and action] 

	`const initialState={
    		counter: 0
	};

	const reducer = (state = initialState, action)=>{
  				return state;
			};

	export default reducer;`

**6-(index.js)**

	`const store=createStore(reducer);`


**7-(terminal)**     
> we created store in redux. Now we need to connect react with redux 

	`npm install --save react-redux`


**8-(index.js)**

	`import { Provider } from "react-redux";`



**9-(index.js)** 	
> we wrap our app with provider 

	`ReactDOM.render(<Provider><App /></Provider>, document.getElementById('root'));`
			         ^       ^ 

**10-(index.js)**	
> Now we add our store which created above to provider as props 

	`<Provider store={store}><App /></Provider> `


**11-(component)**  
> Now we need any component to subscribe to store 

	`import {connect} from "react-redux"; `  


**12-(component)**  
> Go down to exporting that specific component and connect it. [keep first place empty for now] 

	`export default connect()(component)`

**13-(component)**  
> mapping state of redux store to specific components props 

	`const mapStateToProps = state => {
				 	return{	
				           ctr: state.counter 
					};
				};` 

**14-(component)**  
> Fix export default connection. Now component has access to redux counter state as props (readonly) 

	`export default connect(mapStateToProps)(Counter)`       *this.props.ctr -> will show 0 (state in redux)*


**15-(component)** 
> adding dispatch method which will change state of store 

	
	`const mapDispatchToProps = dispatch => {
				 	return{	
				           onIncrementCounter: () => dispatch ({type: "INCREMENT"}) 
					};
				}; `

**16-(component)** 
> fix export default | if we dont need to read and need to just write we should send first argument **null** 

	`export default connect(mapStateToProps, mapDispatchToProps)(Counter)`


**17-(component)** 
> reference this dispatch method to onclick of any element 

	`<div clicked={this.props.onIncrementCounter}> </div>`


**18-(reducer.js)** 
> Now we need to handle this change | rather than default return we will return new increased state 

	

> > (inside reducer, before return default state we add: )  

		
	`const reducer = (state = initialState, action)=>{
				if(action.type === "INCREMENT"){
					return{
						counter: state.counter +1;
					}		
		
				}
  				return state;
			};`

